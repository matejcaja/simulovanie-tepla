#include <stdio.h>
#include <stdlib.h>
#include <GL/freeglut.h>
#include <GL/freeglut_ext.h>

#define TEX_SIZE 256
#define LEFT_EDGE 5
#define RIGHT_EDGE 250
#define HEATER_X1 220
#define HEATER_X2 240
#define HEATER_Y1 50
#define HEATER_Y2 205

typedef struct {
    GLbyte r;
    GLbyte g;
    GLbyte b;
} pixel;

typedef struct{
    int r;
    int g;
    int b;
} scale;

pixel image[TEX_SIZE][TEX_SIZE];
scale heatScales[10];
int temperature[TEX_SIZE][TEX_SIZE];

GLuint texture;
float t = 0;

int getRandNum(int number, int scale){
    return rand() % number + scale;
}

scale addScale(int r,int g, int b){
    scale Scale;

    Scale.r = r;
    Scale.g = g;
    Scale.b = b;

    return Scale;
}

void createHeatScale(){//0 coldest, 9 - hottest
    heatScales[0] = addScale(0,50,255);
    heatScales[1] = addScale(70,160,240);
    heatScales[2] = addScale(120,240,180);
    heatScales[3] = addScale(15,240,120);
    heatScales[4] = addScale(245,190,100);
    heatScales[5] = addScale(245,160,20);
    heatScales[6] = addScale(245,120,80);
    heatScales[7] = addScale(245,70,20);
    heatScales[8] = addScale(245,100,100);
    heatScales[9] = addScale(255,0,0);

}

void initHeat(){
    int x,y;

    //winter is comming!
    for (x=0;x<TEX_SIZE;x++) {
        for (y=0;y<TEX_SIZE;y++) {
            image[x][y].r = heatScales[0].r;
            image[x][y].g = heatScales[0].g;
            image[x][y].b = heatScales[0].b;
            temperature[x][y] = 0;
        }
    }
}

int avoidLessThanZero(int x){
    if((x - 1) <= 0)
        return 0;
    else
        return (x-1);
}

int randomizeDirection(){
    int direction = getRandNum(20,0);

    if(direction < 1)return 0;
    else if(direction > 1 && direction < 6)return 1;
    else if(direction > 6 && direction < 11) return 2;
    else if(direction > 10 && direction < 20) return 3;

    return 3;
}

void setNewTemp(int x1, int y1, int x2, int y2){
    if((y1 < LEFT_EDGE || y1 > RIGHT_EDGE) || (y2 < LEFT_EDGE || y2 > RIGHT_EDGE)){
        int newTemp1 = avoidLessThanZero(temperature[x1][y1]);
        int newTemp2 = avoidLessThanZero(temperature[x2][y2]);

        image[x2][y2].r = abs(heatScales[newTemp2].r);
        image[x2][y2].g = abs(heatScales[newTemp2].g);
        image[x2][y2].b = abs(heatScales[newTemp2].b);

        image[x1][y1].r = abs(heatScales[newTemp1].r);
        image[x1][y1].g = abs(heatScales[newTemp1].g);
        image[x1][y1].b = abs(heatScales[newTemp1].b);

    }else{

        image[x1][y1].r = abs(heatScales[temperature[x1][y1]].r);
        image[x1][y1].g = abs(heatScales[temperature[x1][y1]].g);
        image[x1][y1].b = abs(heatScales[temperature[x1][y1]].b);

        image[x2][y2].r = abs(heatScales[temperature[x2][y2]].r);
        image[x2][y2].g = abs(heatScales[temperature[x2][y2]].g);
        image[x2][y2].b = abs(heatScales[temperature[x2][y2]].b);
    }

}

void manageDirection(int x1, int y1, int x2, int y2){
    int temp = 0;
    int a = temperature[x1][y1];
    int b = temperature[x2][y2];

    if(a > b){
        temp = abs((a - b) - (rand() % (a==0?1:a)));
        temperature[x2][y2] = temp;

        setNewTemp(x2,y2,x1,y1);
    }else if(b > a){
        temp = abs((b - a) - (rand() % (b==0?1:b)));
        temperature[x1][y1] = temp;
        setNewTemp(x1,y1,x2,y2);
    }
}



//keep bottom warm
void heater(){
    int x,y;

    for (x=HEATER_X1;x<HEATER_X2;x++) {
            for (y=HEATER_Y1;y<HEATER_Y2;y++) {
                image[x][y].r = heatScales[9].r;
                image[x][y].g = heatScales[9].g;
                image[x][y].b = heatScales[9].b;
                temperature[x][y] = 9;
            }
        }
}

void changeTemp(){
    int x,y;

    for(x = TEX_SIZE-1; x >= 0; x--){
            for(y = TEX_SIZE-1; y >= 0; y--){
                int direction = randomizeDirection();
                //int direction = 3;

                if(direction == 3){//case up
                    if(x > 0){
                        manageDirection(x, y, x-1,y);
                    }
                }
                else if(direction == 2){//case right
                    if(y < 255){
                        manageDirection(x, y, x,y+1);
                    }

                }else if(direction == 1){//case left
                    if(y > 0){
                        manageDirection(x, y, x,y-1);
                    }
                }else if(direction == 0){//case bottom
                    if(x < 255){
                        manageDirection(x, y, x+1,y);
                    }
                }
        }
    }
}

// Initialize OpenGL state
void init() {
	// Texture setup
    glEnable(GL_TEXTURE_2D);
    glGenTextures( 1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    // Other
    glClearColor(0,0,0,0);
    gluOrtho2D(-1,1,-1,1);
    //glLoadIdentity();
    glColor3f(1,1,1);
}

// Generate and display the image.
void display() {
    createHeatScale();
    initHeat();

    while(1){
        heater();
        changeTemp();
        // Copy image to texture memory
        glBindTexture(GL_TEXTURE_2D, texture);
        //Load image
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TEX_SIZE, TEX_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        // Clear screen buffer
        glClear(GL_COLOR_BUFFER_BIT);
        // Render a quad

          glBegin(GL_QUADS);
            glTexCoord2f(0,1); glVertex2f(-1,-1);
            glTexCoord2f(1,1); glVertex2f(1,-1);
            glTexCoord2f(1,0); glVertex2f(1,1);
            glTexCoord2f(0,0); glVertex2f(-1,1);
          glEnd();

        // Display result
        glFlush();
        glutPostRedisplay();
        glutSwapBuffers();

    }

}

void update() {
  t += 0.1;
  glutPostRedisplay();
}

// Main entry function
int main(int argc, char ** argv) {
    // Init GLUT
    glutInit(&argc, argv);
    glutInitWindowSize(TEX_SIZE, TEX_SIZE);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutCreateWindow("Simulation");
    // Set up OpenGL state
    init();
    // Run the control loop
    glutDisplayFunc(display);

    glutMainLoop();

    return EXIT_SUCCESS;

}
